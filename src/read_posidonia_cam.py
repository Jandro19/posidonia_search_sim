#!/usr/bin/env python

"""
This ROS node receives live camera images through the topic specified on /posidonia/read_cam/name rosparam.
The image is processed and binarized to mimic the possible output of a deep learning module detecting posidonia.
The ending binary matrix is published under /posidonia/ocupation_mtx topic
Debug published topics: /posidonia/image_masked , /posidonia/edge_detection
"""



import sys
import rospy
import numpy as np
from scipy import ndimage
from itertools import izip
# Import needed messages
from sensor_msgs.msg import Image
from std_msgs.msg import UInt8MultiArray, MultiArrayLayout, MultiArrayDimension
from posidonia_search_sim.msg import EdgeDetected
from cv_bridge import CvBridge

from math import atan2

bold_color = '\033[1m'
end_color = '\033[0m'


class ReadPosidoniaCam(object):
    def __init__(self, robot):
        if robot not in ("sparus2", "girona500"):
            raise Exception("invalid robot", robot)

        self.posidonia_mask_msg = None
        self.img_data = None
        self.cv_bridge = CvBridge()

        # Publisher
        self.pub_mask = rospy.Publisher("/{:s}/posidonia/ocupation_mtx".format(robot), UInt8MultiArray, queue_size=1)
        self.pub_img = rospy.Publisher("/{:s}/posidonia/image_masked".format(robot), Image, queue_size=1)
        self.pub_edge_detection = rospy.Publisher("/{:s}/posidonia/edge_detection".format(robot), EdgeDetected, queue_size=1)

        try:
            cam_name = rospy.get_param("/{:s}/posidonia/read_cam/name".format(robot))
        except:
            raise Exception("No cam name rosparam in /{:s}/posidonia/read_cam/name".format(robot))

        # Subscriber
        self.sub_camera = rospy.Subscriber("/{:s}{:s}".format(robot,cam_name), Image, self.read_image_from_cam)

        print(bold_color + rospy.names.get_name() + ":" + end_color +
              " Reading cam topic from {:s}".format(cam_name))

    def read_image_from_cam(self, img):
        # Deserialize msg into a ndarray
        self.img_data = self.cv_bridge.imgmsg_to_cv2(img, desired_encoding="passthrough")
        # self.img_data.setflags(write=1)  # By default is read only data
        self.img_header = img.header

    def iterate(self):
        if self.img_data is not None:
            # Create posidonia mask array
            posidonia_mask = np.zeros((self.img_data.shape[0], self.img_data.shape[1]), dtype=np.uint8)
            for i, row in enumerate(self.img_data):
                for j, pixel in enumerate(row):
                    # blue value threshold or red and green high
                    if pixel[2] < 130 or pixel[1] > 175:  # low blue or high green
                        posidonia_mask[i, j] = 1

            # Process mask (denoise, open&close, etc)
            posidonia_mask = self.process_mask(posidonia_mask)

            # Filter image through mask
            data = self.img_data.copy()
            for i, row in enumerate(self.img_data):
                for j, pixel in enumerate(row):
                    if posidonia_mask[i, j] == 0:
                        data[i, j] = [0, 0, 0]

            # Create mask if not already created
            if self.posidonia_mask_msg is None:
                rows = posidonia_mask.shape[0]
                cols = posidonia_mask.shape[1]
                dim = [MultiArrayDimension(), MultiArrayDimension()]

                dim[0].label = "height"
                dim[0].size = rows
                dim[0].stride = rows * cols
                dim[1].label = "width"
                dim[1].size = cols
                dim[1].stride = cols * 1

                layout = MultiArrayLayout()
                layout.dim = dim

                self.posidonia_mask_msg = UInt8MultiArray()
                self.posidonia_mask_msg.layout = layout
            self.posidonia_mask_msg.data = posidonia_mask.flatten().tolist()

            # Find edge, if exists and draw it in data
            self.edge_detection(posidonia_mask, data)

            # Create img masked msg
            posidonia_image_masked_msg = self.cv_bridge.cv2_to_imgmsg(data, "rgb8")
            posidonia_image_masked_msg.header = self.img_header

            if self.posidonia_mask_msg is not None:
                self.pub_mask.publish(self.posidonia_mask_msg)

            if posidonia_image_masked_msg is not None:
                self.pub_img.publish(posidonia_image_masked_msg)

    def edge_detection(self, posidonia_mask, data):
        """
        Scans the image and sends a message if possidonia is detected
        Also for debug purposes, finds the center of the detection and draw a line over the edge
        """
        mask = posidonia_mask

        # Find center of mass of the positive and negative detection
        sum_det_i = 0
        sum_det_j = 0
        sum_no_det_i = 0
        sum_no_det_j = 0
        count_det = 0
        count_no_det = 0
        for i, row in enumerate(mask):
            for j, value in enumerate(row):
                if value == 1:
                    sum_det_i += i
                    sum_det_j += j
                    count_det += 1
                else:
                    sum_no_det_i += i
                    sum_no_det_j += j
                    count_no_det += 1

        # Don't search when there is too much or too little posidonia
        size = mask.shape[0] * mask.shape[1]
        if count_det < size * 0.05 or count_det > size * 0.7:
            return

        if count_det > 0 and count_no_det > 0:

            hard_edge = self.edge_detection_2(posidonia_mask, data)

            if hard_edge is not None:
                self.send_edge_message(hard_edge[0], hard_edge[1], hard_edge[2])
                return

            det_center_i = int(round(sum_det_i / float(count_det)))
            det_center_j = int(round(sum_det_j / float(count_det)))

            # show center in data
            self.draw_center_1(data, det_center_i, det_center_j)

            no_det_center_i = int(round(sum_no_det_i / float(count_no_det)))
            no_det_center_j = int(round(sum_no_det_j / float(count_no_det)))
            self.draw_center_3(data, no_det_center_i, no_det_center_j)

            # get angle between centers
            a1 = no_det_center_i - det_center_i
            a2 = no_det_center_j - det_center_j

            # transform point coords to have (0,0) at the center of the image
            p_x = no_det_center_j - mask.shape[1]/2
            p_y = no_det_center_i - mask.shape[0]/2
            self.send_edge_message(p_x, p_y, atan2(a2,a1))

            if a1 == 0:
                m = None
            else:
                m = -float(a2)/float(a1)

            if m is not None:
                # Paint edge line from point to the right
                idx_i = no_det_center_i
                idx_j = no_det_center_j
                value = m

                while 0 < idx_i < data.shape[0] and 0 < idx_j < data.shape[1]:
                    data[idx_i, idx_j] = [0,255,0]
                    if value < m:
                        if m > 0:
                            idx_i += 1
                            value += 1 / m
                        else:
                            idx_i -= 1
                            value += -1/m
                        if abs(m) < 1:
                            idx_j += 1
                    else:
                        idx_j += 1
                        value -= 1
                        if abs(m) >= 1:
                            if m > 0:
                                idx_i += 1
                            else:
                                idx_i -= 1

                # Paint edge line from point to the left
                idx_i = no_det_center_i
                idx_j = no_det_center_j
                value = m

                while 0 < idx_i < data.shape[0] and 0 < idx_j < data.shape[1]:
                    data[idx_i, idx_j] = [0, 255, 0]
                    if value < m:
                        if m > 0:
                            idx_i -= 1
                            value += 1 / m
                        else:
                            idx_i += 1
                            value += -1 / m
                        if abs(m) < 1:
                            idx_j -= 1
                    else:
                        idx_j -= 1
                        value -= 1
                        if abs(m) >= 1:
                            if m > 0:
                                idx_i -= 1
                            else:
                                idx_i += 1

    def edge_detection_2(self, posidonia_mask, data):
        """
        In case there is posidonia at the top right corner of the image while following the edge,
        it could mean that there is ahead another patch near the current one, draw the edge line
        connecting both patches so the uav can follow it
        For debug purposes, draw it over the image to view in rviz
        """

        # if there is more posidonia detected than threshold in the top right quadrant, find egde
        q_shape = (posidonia_mask.shape[0]/3, posidonia_mask.shape[1]/2)
        cont = 0
        for row in posidonia_mask[:q_shape[0]]:
            for value in row[q_shape[1]:]:
                if value == 1:
                    cont += 1

        total = q_shape[0]*q_shape[1]
        if float(cont) / total < 0.05:
            return None

        # Get vector of the right most detection of every row
        edge_vector = np.zeros(posidonia_mask.shape[0], dtype=np.int16)
        for i, row in enumerate(posidonia_mask):
            reverse_enumerate = izip(xrange(len(row) - 1, -1, -1), reversed(row))
            for j, value in reverse_enumerate:
                if value == 1:
                    edge_vector[i] = j
                    break

        # find the right most detection of the top right quadrant
        p1 = (0, 0)
        for x, y in enumerate(edge_vector[:posidonia_mask.shape[0]/3]):
            if y > posidonia_mask.shape[1]/2 and y >= p1[1]:
                p1 = (x, y)

        # if none found, do nothing
        if p1 == (0, 0):
            return None

        # find the next detection point under p1 with the least slope to set an edge
        slope = None
        for x, y in enumerate(edge_vector):
            if x > p1[0]:
                a1 = x - p1[0]
                a2 = y - p1[1]
                if a2 == 0:
                    m = 0
                else:
                    m = float(a1)/float(a2)
                if slope is None or m < slope:
                    slope = m
                    p2 = (x, y)

        self.draw_center_2(data,p1[0],p1[1])
        self.draw_center_4(data,p2[0],p2[1])
        # Paint line over data
        if slope is not None and slope != 0:
            m = slope
            # Paint edge line from point to the right
            idx_i = p1[0]
            idx_j = p1[1]
            value = m

            while 0 < idx_i < data.shape[0] and 0 < idx_j < data.shape[1]:
                data[idx_i, idx_j] = [0, 255, 255]
                if value < m:
                    if m > 0:
                        idx_i += 1
                        value += 1 / m
                    else:
                        idx_i -= 1
                        value += -1 / m
                    if abs(m) < 1:
                        idx_j += 1
                else:
                    idx_j += 1
                    value -= 1
                    if abs(m) >= 1:
                        if m > 0:
                            idx_i += 1
                        else:
                            idx_i -= 1

            # Paint edge line from point to the left
            idx_i = p1[0]
            idx_j = p1[1]
            value = m

            while 0 < idx_i < data.shape[0] and 0 < idx_j < data.shape[1]:
                data[idx_i, idx_j] = [0, 255, 255]
                if value < m:
                    if m > 0:
                        idx_i -= 1
                        value += 1 / m
                    else:
                        idx_i += 1
                        value += -1 / m
                    if abs(m) < 1:
                        idx_j -= 1
                else:
                    idx_j -= 1
                    value -= 1
                    if abs(m) >= 1:
                        if m > 0:
                            idx_i -= 1
                        else:
                            idx_i += 1

        a1 = p1[0] - p2[0]
        a2 = p1[1] - p2[1]
        angle = atan2(-a1, a2)

        return p1[1] - q_shape[1], p1[0] - q_shape[0], angle

    def send_edge_message(self, offset_x, offset_y, direction):
        """ Sends a detected edge message """
        msg = EdgeDetected()
        msg.direction = direction
        msg.offset_x = offset_x
        msg.offset_y = offset_y

        self.pub_edge_detection.publish(msg)

    def draw_center_1(self, data, i, j):
        """ Draws a red marker over the provided point """
        if 3 < i < data.shape[0] and 3 < j < data.shape[1]:
            data[i,   j] = [255,0,0]
            data[i-1, j] = [255,0,0]
            data[i+1, j] = [255,0,0]
            data[i-1, j-1] = [255,0,0]
            data[i,   j-1] = [255,0,0]
            data[i+1, j-1] = [255,0,0]
            data[i-1, j+1] = [255,0,0]
            data[i,   j+1] = [255,0,0]
            data[i+1, j+1] = [255,0,0]
            data[i-2, j] = [255,0,0]
            data[i+2, j] = [255,0,0]
            data[i,   j-2] = [255,0,0]
            data[i,   j+2] = [255,0,0]

    def draw_center_2(self, data, i, j):
        """ Draws a yellow marker over the provided point """
        if 3 < i < data.shape[0] and 3 < j < data.shape[1]:
            data[i,   j] = [255,255,0]
            data[i-1, j] = [255,255,0]
            data[i+1, j] = [255,255,0]
            data[i,   j-1] = [255,255,0]
            data[i,   j+1] = [255,255,0]
            data[i-2, j] = [255,255,0]
            data[i+2, j] = [255,255,0]
            data[i,   j-2] = [255,255,0]
            data[i,   j+2] = [255,255,0]

    def draw_center_4(self, data, i, j):
        """ Draws a magenta marker over the provided point """
        if 3 < i < data.shape[0] and 3 < j < data.shape[1]:
            data[i,   j] = [255,0,255]
            data[i-1, j] = [255,0,255]
            data[i+1, j] = [255,0,255]
            data[i,   j-1] = [255,0,255]
            data[i,   j+1] = [255,0,255]
            data[i-2, j] = [255,0,255]
            data[i+2, j] = [255,0,255]
            data[i,   j-2] = [255,0,255]
            data[i,   j+2] = [255,0,255]

    def draw_center_3(self, data, i, j):
        """ Draws a green marker over the provided point """
        if 3 < i < data.shape[0] and 3 < j < data.shape[1]:
            data[i,   j] = [0,0,0]
            data[i-1, j] = [0,255,0]
            data[i+1, j] = [0,255,0]
            data[i,   j-1] = [0,255,0]
            data[i,   j+1] = [0,255,0]
            data[i-2, j] = [0,255,0]
            data[i+2, j] = [0,255,0]
            data[i,   j-2] = [0,255,0]
            data[i,   j+2] = [0,255,0]

    def process_mask(self, mask):
        """ Reduces noise, holes and adds realism to the mask """
        structure = [[0,0,1,0,0],
                     [0,1,1,1,0],
                     [1,1,1,1,1],
                     [0,1,1,1,0],
                     [0,0,1,0,0]]


        # mask = ndimage.binary_e
        mask = ndimage.binary_dilation(mask, structure)
        # mask = ndimage.binary_dilation(mask, structure)
        mask = ndimage.binary_erosion(mask, structure)
        mask = ndimage.binary_opening(mask, structure)

        mask = mask.astype(np.uint8)
        return mask


if __name__ == '__main__':

    # default robot
    robot = "girona500"

    # input
    if len(sys.argv) >= 2:
        robot = sys.argv[1]

    # init node
    rospy.init_node("read_posidonia_cam")
    node = ReadPosidoniaCam(robot)
    r = float(rospy.get_param("/{:s}/posidonia/read_cam/rate".format(robot)))
    rate = rospy.Rate(r)

    while not rospy.is_shutdown():
        node.iterate()
        rate.sleep()
