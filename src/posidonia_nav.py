#!/usr/bin/env python
from __future__ import print_function
import sys
import rospy
import numpy as np
import __builtin__

from math import sqrt, sin, cos, radians, pi, exp, atan2
from geometry_msgs.msg import Point
from actionlib_msgs.msg import GoalID, GoalStatusArray
from cola2_msgs.msg import NavSts, WorldSectionActionGoal, WorldSectionGoal, WorldSectionActionResult, BodyVelocityReq
from posidonia_search_sim.msg import EdgeDetected
from map_msgs.msg import OccupancyGridUpdate

bold_color = '\033[1m'
end_color = '\033[0m'

# weighting factor, dist vs angle
weighting_factor = 10
candidate_offset = 1  # in meters

class PosidoniaNav(object):
    def __init__(self, robot):
        if robot not in ("sparus2", "girona500"):
            raise Exception("invalid robot", robot)

        # Read rosparams
        try:
            self.lm_elongation = float(rospy.get_param("/{:s}/posidonia/lawnmower/elongation".format(robot)))
            self.lm_separation = float(rospy.get_param("/{:s}/posidonia/lawnmower/separation".format(robot)))
            self.lm_iterations = int(rospy.get_param("/{:s}/posidonia/lawnmower/iterations".format(robot)))
            self.lm_inclination = float(rospy.get_param("/{:s}/posidonia/lawnmower/inclination".format(robot)))
            self.lm_iter_direction = int(rospy.get_param("/{:s}/posidonia/lawnmower/direction".format(robot)))
            self.altitude_mode = bool(rospy.get_param("/{:s}/posidonia/lawnmower/altitude_mode".format(robot)))
            self.fixed_altitude = float(rospy.get_param("/{:s}/posidonia/lawnmower/fixed_altitude".format(robot)))
        except:
            raise Exception("Could not read /{:s}/posidonia/lawnmower/ rosparams".format(robot))

        self.mask = None
        self.position = None
        self.orientation = None
        self.action = None
        self.lm_msg = None
        self.scan_msg = None
        self.idle = False
        self.lm_points = []
        self.lm_current_point = None
        self.last_used_id = None
        self.posidonia_direction = None
        self.posidonia_quantity = None
        self.scanning_main_direction = None
        self.scanning_step = None
        self.cell_size = None
        self.executing_separation_step = False
        self.last_step_distance = 0
        self.step_posidonia_distance = 0
        self.step_ini_p = None
        self.candidate_vp = None
        self.last_viewpoint_angle = None
        self.state = "Lawnmower"
        self.lm_direction_ew = -1  # East = 1, west = -1. This is the width direction.
        self.lm_direction_ns = -1  # North = 1, South = -1. This is the height direction.
        self.lm_main_direction = self.lm_direction_ns  # This is the direction that will be alternating for the lawnmower
        self.lm_separation_direction = self.lm_direction_ew  # This direction will stay constant

        # Subscriptions
        self.sub_navsts = rospy.Subscriber("/{:s}/navigator/navigation".format(robot), NavSts, self.read_navsts)
        self.sub_action_result = rospy.Subscriber("/{:s}/pilot/world_section_req/result".format(robot),
                                                  WorldSectionActionResult, self.action_finalised)
        self.sub_action_status = rospy.Subscriber("/{:s}/pilot/world_section_req/status".format(robot),
                                                  GoalStatusArray, self.read_action_status)
        self.sub_update = rospy.Subscriber("/{:s}/posidonia/occupancy_grid_updates".format(robot),
                                           OccupancyGridUpdate, self.find_viewpoints)
        self.sub_edge_detection = rospy.Subscriber("/{:s}/posidonia/edge_detection".format(robot), EdgeDetected,
                                                   self.edge_detected)

        # Publications
        self.pub_vel_req = rospy.Publisher("/{:s}/controller/body_velocity_req".format(robot), BodyVelocityReq,
                                           queue_size=1)
        self.pub_goal = rospy.Publisher("/{:s}/pilot/world_section_req/goal".format(robot), WorldSectionActionGoal,
                                   queue_size=1)
        self.pub_cancel = rospy.Publisher("/{:s}/pilot/world_section_req/cancel".format(robot), GoalID, queue_size=1)

        # Cancel current action to avoid problems
        self.cancel_current_section_goal()

    def read_navsts(self, navsts):
        """ Reads navstats message """
        self.position = navsts.position
        self.orientation = navsts.orientation
        self.altitude = navsts.altitude

    def action_finalised(self, result):
        """
        Detects when an action ends, depending on the result will print messages
        and execute the nexet lawnmower point
        """
        if result.status.status == 3:
            print("Action ended correctly")
        elif result.status.status == 2:
            print("Action was cancelled")
        elif result.status.status == 4:
            # unknown error, repeat msg
            print("status 4, repeating section")
            self.scan_msg = None
            return

        if self.state == "Lawnmower":
            # Prepare next lm iteration
            self.lm_msg = None
            self.lm_main_direction *= -1  # So the next iteration will go the opposite way

    def read_action_status(self, status):
        """ Reads the status of the current vehicle action """
        if len(status.status_list) == 0:
            self.last_used_id = 1
            self.idle = True
        else:
            last_status = status.status_list[len(status.status_list)-1]
            self.last_used_id = int(last_status.goal_id.id)
            if last_status.status == 1:
                self.idle = False
            else:
                self.idle = True

    def find_viewpoints(self, update_msg):
        """ Find the candidates to explore and select the best one """
        if self.state == "Following edge":
            if self.cell_size is None:
                self.cell_size = float(rospy.get_param("/{:s}/posidonia/map_posidonia/cell_size".format(robot)))

            width = update_msg.width
            height = update_msg.height
            map = np.reshape(update_msg.data, (height, width))
            map = map[::-1, ::-1].T  # Anti transpose matrix

            # Find candidates from map
            candidates = self.find_candidates(map)

            # Robot is in the center of the map, so
            robot_cell = (height/2.0 + 0.5, width/2.0 + 0.5)

            # Find best candidate
            if len(candidates) != 0:
                candidate = self.find_best_candidate(candidates, robot_cell)
                world_can = self.point_from_map_to_world(candidate, robot_cell)

                # Offset candidate into the sea, so the robot can better follow the edge
                c_north = world_can[0] + cos(candidate[2]) * candidate_offset
                c_east = world_can[1] + sin(candidate[2]) * candidate_offset

                self.candidate_vp = [c_north, c_east, candidate[2]]

            else:
                self.candidate_vp = None

    def find_candidates(self, map):
        """
        Finds candidates in 8-neighbours surrounding the candidate pixel
        Candidates can be posidonia cells that have as neighbours at least one empty
        cell and one unexplored cell
        """
        candidates = []
        for row in range(1, map.shape[0]-1):
            for col in range(1, map.shape[1]-1):
                # If a posidonia cell
                if map[row][col] > 0:
                    # Has as neighbours an empty and an unexplored cell:
                    neighbourhood = map[row-1:row+2, col-1:col+2]
                    if -1 in neighbourhood and 0 in neighbourhood:
                        # It is a candidate

                        # Find candidate angle towards empty cells from center of detection cells and
                        # center of empty cells within the neighbourhood
                        empty_c_x = 0.0
                        empty_c_y = 0.0
                        empty_cont = 0
                        det_c_x = 0.0
                        det_c_y = 0.0
                        det_cont = 0
                        for i, fila in enumerate(neighbourhood):
                            for j, valor in enumerate(fila):
                                if valor == 0:  # If cell is empty
                                    empty_cont += 1
                                    empty_c_x += i
                                    empty_c_y += j
                                if valor > 0:  # If cell has detection
                                    det_cont += 1
                                    det_c_x += i
                                    det_c_y += j

                        # Get center of empty cells
                        empty_c_x = empty_c_x / empty_cont
                        empty_c_y = empty_c_y / empty_cont

                        # Get center of detection cells
                        det_c_x = det_c_x / det_cont
                        det_c_y = det_c_y / det_cont

                        # Angle between the center detection point and surrounding empty space
                        angle = -1 * atan2(det_c_y - empty_c_y, det_c_x - empty_c_x)

                        # Filter isolated candidates, probably noise
                        if det_cont > 1:
                            candidates.append((row, col, angle))

        return candidates

    def find_best_candidate(self, candidates, robot_cell):
        """ Following Dr Vidal's paper, apply a weight function to each candidate to find the best one """
        cost = None
        candidate = None
        for can in candidates:
            b = self.wrap_pi(-atan2(robot_cell[1] - can[1], robot_cell[0] - can[0]) - self.orientation.yaw)
            d = sqrt((can[1] - robot_cell[1])**2 + (can[0] - robot_cell[0])**2)
            k = abs(pi/2 - b)/(pi/2)
            c = d + weighting_factor * k

            if cost is None or c < cost:
                cost = c
                candidate = can

        return candidate

    def edge_detected(self, detection):
        """ When an edge message is detected, change state to 'Following edge' """
        self.edge_direction = detection.direction
        self.edge_x_offset = detection.offset_x
        self.edge_y_offset = detection.offset_y

        if self.state == "Lawnmower":
            self.state = "Following edge"
            self.cancel_current_section_goal()
            print("Following edge")

    def iterate(self):
        """
        Main logic. When in lawnmower, keep sending section messages,
        when in following edge, keep updating and sending velocity requests
        """
        if self.state == "Lawnmower":
            if self.posidonia_quantity is not None and self.posidonia_quantity > detection_threshold:
                # Found a patch, stop lawnmower and start searching the posidonia patch
                print("Posidonia patch detected. Starting scan.")
                self.cancel_current_section_goal()
                self.lm_msg = None
                self.state = "Scanning patch"
                self.scanning_step = 1
                self.scanning_main_direction = self.posidonia_direction + self.orientation.yaw
                print(self.posidonia_direction, self.orientation.yaw, self.scanning_main_direction)

            else:
                self.process_lawnmower()
                if self.lm_msg is not None:
                    self.pub_goal.publish(self.lm_msg)

        elif self.state == "Following edge":
            if self.candidate_vp is None:
                self.align_with_edge()

            else:
                self.follow_candidate()

    def align_with_edge(self):
        """ Aligns vehicle with edge message. Called when no candidates are found """

        msg = BodyVelocityReq()
        msg.header.stamp = rospy.Time.now()
        msg.goal.requester = "posidonia_nav"
        msg.goal.priority = msg.goal.PRIORITY_NORMAL

        # Correct direction from angle 0 = right to angle 0 = forward
        direction = self.edge_direction - pi/2

        # hover over edge 'point', maintaining a Y offset between (10, 20), so edge will be always on the right side

        msg.twist.linear.x = exp(-abs(direction)) * 0.5
        msg.twist.linear.y = (self.edge_x_offset - 15) / 200
        msg.twist.linear.z = 0.0
        # msg.twist.angular.x = 0.0
        # msg.twist.angular.y = 0.0
        msg.twist.angular.z = -(self.edge_direction - pi/2) / 10 + msg.twist.linear.y
        msg.disable_axis.x = False
        msg.disable_axis.y = False
        msg.disable_axis.z = True
        msg.disable_axis.roll = True
        msg.disable_axis.pitch = True
        msg.disable_axis.yaw = False

        self.pub_vel_req.publish(msg)

    def follow_candidate(self):
        """ Updates and sends velocity request message to align vehicle with the viewpoint of the candidate """
        msg = BodyVelocityReq()
        msg.header.stamp = rospy.Time.now()
        msg.goal.requester = "posidonia_nav"
        msg.goal.priority = msg.goal.PRIORITY_NORMAL

        # Calc linear and angular distance to point
        linear_d = sqrt((self.position.north - self.candidate_vp[0])**2 + (self.position.east-self.candidate_vp[1])**2)
        angular_d = self.wrap_pi(self.orientation.yaw - (self.candidate_vp[2]-pi/2))

        ang_robot_point = atan2(self.candidate_vp[1]-self.position.east, self.candidate_vp[0]-self.position.north)
        angular_d = self.wrap_pi(ang_robot_point - self.orientation.yaw)

        # Linear velocity on X (forward)
        vlx = linear_d * exp(-abs(2 * angular_d)**2)
        if vlx > 0.5:
            vlx = 0.5
        elif vlx < 0.1:
            vlx = 0.0

        # Linear velocity on Y (sideways)
        vly = 0.0

        # Angular velocity on Z (Yaw)
        vaz = angular_d / 5
        if vaz > 0.4:
            vaz = 0.4
        elif vaz < -0.4:
            vaz = -0.4

        msg.twist.linear.x = vlx
        msg.twist.linear.y = vly
        msg.twist.linear.z = 0.0
        msg.twist.angular.z = vaz
        msg.disable_axis.x = False
        msg.disable_axis.y = False
        msg.disable_axis.z = True
        msg.disable_axis.roll = True
        msg.disable_axis.pitch = True
        msg.disable_axis.yaw = False

        self.pub_vel_req.publish(msg)

    def process_lawnmower(self):
        """ Create and update lawnmower points and current point """
        if len(self.lm_points) == 0:  # Lawnmower points need to be generated
            self.generate_lm_points()
            self.lm_current_point = -1

        elif self.lm_msg is None and self.idle:  # Create next lawnmower message
            self.lm_current_point += 1
            if self.lm_current_point < len(self.lm_points)-1:  # If there are 4 points, only 3 sections will be done
                ini_p = self.lm_points[self.lm_current_point]
                final_p = self.lm_points[self.lm_current_point+1]
                self.lm_msg = self.create_section_msg(ini_p, final_p)
                print("Created lawnmower section message for point", self.lm_current_point)
            else:
                print("Lawnmower ended")
                rospy.signal_shutdown("End of program")

    def get_current_point(self):
        """ Creates a point geometry msg from the current position """
        x1 = self.position.north
        y1 = self.position.east
        if self.altitude_mode:
            z1 = self.altitude
        else:
            z1 = self.position.depth

        return Point(x1, y1, z1)

    def generate_lm_points(self):
        """ Generates lawnmower points depending on the specified rosparams """
        if self.position is not None:
            self.lm_points = list()

            # Create first 2 points of the lawnmower
            x1 = self.position.north
            y1 = self.position.east
            if self.altitude_mode:
                z1 = self.fixed_altitude
            else:
                z1 = self.position.depth

            first_point = Point(x1, y1, z1)
            self.lm_points.append(first_point)

            x2 = x1 + sin(radians(self.lm_inclination))*self.lm_elongation
            y2 = y1 + cos(radians(self.lm_inclination))*self.lm_elongation
            z2 = z1
            second_point = Point(x2, y2, z2)
            self.lm_points.append(second_point)

            # Create rest of points depending on the number of iterations. Each iteration creates 2 points, one for
            # the separation section and one for the next along section.
            for i in range(1, self.lm_iterations):
                # Go up or down
                last_point = self.lm_points[len(self.lm_points)-1]
                x = last_point.x
                y = last_point.y
                z = last_point.z
                angle = self.wrap_360_angle(self.lm_inclination + 90 * self.lm_iter_direction)

                x = x + sin(radians(angle))*self.lm_separation
                y = y + cos(radians(angle))*self.lm_separation

                p = Point(x, y, z)
                self.lm_points.append(p)

                # Go along the other direction
                if i % 2 == 1:  # direction contrary to original inclination
                    x = x - sin(radians(self.lm_inclination))*self.lm_elongation
                    y = y - cos(radians(self.lm_inclination))*self.lm_elongation
                else:  # direction along original inclination
                    x = x + sin(radians(self.lm_inclination))*self.lm_elongation
                    y = y + cos(radians(self.lm_inclination))*self.lm_elongation

                p = Point(x, y, z)
                self.lm_points.append(p)
            print("Generated lawnmower points")

    def create_section_msg(self, ini_p, final_p):
        """ Creates a section message """
        if self.last_used_id is not None:
            msg = WorldSectionActionGoal()
            msg.goal_id.id = str(self.last_used_id + 1)
            goal = WorldSectionGoal()
            goal.initial_position = ini_p
            goal.final_position = final_p
            goal.tolerance = Point(0.5, 0.5, 0.5)
            goal.altitude_mode = self.altitude_mode
            goal.disable_z = False
            goal.priority = 10
            distance = sqrt((ini_p.x-final_p.x)**2 + (ini_p.y-final_p.y)**2 + (ini_p.z-final_p.z)**2)
            goal.timeout = distance * 10
            if goal.timeout < 30:
                goal.timeout = 30  # Establish a minimum timeout
            goal.surge_velocity = 1.0
            msg.goal = goal

            return msg

    def cancel_current_section_goal(self):
        """ Cancels current section goal """
        print("Cancelling current goal")
        for i in range(0,10):
            self.pub_cancel.publish(GoalID())
            rospy.Rate(10).sleep()

    def point_from_map_to_world(self, point, robot_cell):
        """ Translate point from cell map to world coordinates """
        p0 = - (point[0] - robot_cell[0]) * self.cell_size + self.position.north
        p1 =   (point[1] - robot_cell[1]) * self.cell_size + self.position.east
        p2 =    point[2]
        p = (p0, p1, p2)
        return p

    def wrap_360_angle(self, angle):
        """ Wrap angle from 0 to 360 degrees"""
        return angle % 360

    def wrap_pi(self, angle):
        """ Converts an angle to a value contained within the range (-pi, pi] """
        angle = angle % (2*pi)
        angle = (angle + 2*pi) % (2*pi)
        if angle > pi:
            angle -= 2*pi

        return angle

def print(*args):
    """ Custom print to show name of the node """
    if len(args) == 1:
        __builtin__.print(bold_color + rospy.names.get_name() + ": " + end_color + str(args[0]))
    else:
        __builtin__.print(bold_color + rospy.names.get_name() + ": " + end_color + str(args))

if __name__ == '__main__':
    # default robot
    robot = "girona500"

    # input
    if len(sys.argv) <= 2:
        robot = sys.argv[1]

    # init node
    rospy.init_node("posidonia_nav")
    node = PosidoniaNav(robot)
    rate = rospy.Rate(10.0)

    # Wait map node initialization
    while not bool(rospy.get_param("/{:s}/posidonia/map_posidonia/init_done".format(robot))):
        rate.sleep()

    while not rospy.is_shutdown():
        node.iterate()
        rate.sleep()
