#!/usr/bin/env python

"""
This node stores information of the mapped area and detections of possidonia
Publishes periodically a local map around the AUV
"""

from __future__ import print_function
import sys
import rospy
import struct
import tf
import __builtin__

import numpy as np

from nav_msgs.msg import OccupancyGrid
from map_msgs.msg import OccupancyGridUpdate
from std_msgs.msg import UInt8MultiArray
from cola2_msgs.msg import NavSts
from math import sin, cos, tan, radians

bold_color = '\033[1m'
end_color = '\033[0m'

class MapPosidonia(object):
    def __init__(self, robot):

        print("Initializing and populating map...")

        self.robot = robot

        # Init ros stuff
        self.__init_ros__(robot)

        # Init map stuff
        self.__init_map__()

        print("Done. Unexplored map sent.")

        # Set param signaling end of initialization
        rospy.set_param("/{:s}/posidonia/map_posidonia/init_done".format(robot), "True")

    def __init_ros__(self, robot):
        if robot not in ("sparus2", "girona500"):
            raise Exception("invalid robot", robot)

        # Get rosparams
        try:
            self.underwater_fov_h = float(rospy.get_param("/{:s}/posidonia/map_posidonia/underwater_fov_h".format(robot)))
            self.underwater_fov_v = float(rospy.get_param("/{:s}/posidonia/map_posidonia/underwater_fov_h".format(robot)))
            self.cam_altitude = float(rospy.get_param("/{:s}/posidonia/map_posidonia/altitude".format(robot)))
            self.n_cells_h = int(rospy.get_param("/{:s}/posidonia/map_posidonia/n_cells_h".format(robot)))
            self.n_cells_v = int(rospy.get_param("/{:s}/posidonia/map_posidonia/n_cells_v".format(robot)))
            self.h_map_size = float(rospy.get_param("/{:s}/posidonia/map_posidonia/h_map_size".format(robot)))
            self.v_map_size = float(rospy.get_param("/{:s}/posidonia/map_posidonia/v_map_size".format(robot)))
            self.update_radius = int(rospy.get_param("/{:s}/posidonia/map_posidonia/update_radius".format(robot)))
        except:
            raise Exception("Failed to load rosparams")

        self.initial_publish = False

        # Subscribers
        self.sub_mask = rospy.Subscriber("/{:s}/posidonia/ocupation_mtx".format(robot), UInt8MultiArray, self.read_mask)
        self.sub_navsts = rospy.Subscriber("/{:s}/navigator/navigation".format(robot), NavSts, self.read_navsts)

        # Publishers
        self.pub_grid = rospy.Publisher("/{:s}/posidonia/occupancy_grid".format(robot), OccupancyGrid, queue_size=1)
        self.pub_upd_grid = rospy.Publisher("/{:s}/posidonia/occupancy_grid_updates".format(robot), OccupancyGridUpdate, queue_size=1)

        # Transform broadcasters
        self.map_tf_br = tf.TransformBroadcaster()

        # Variables for the ros subscriptions
        self.position = None
        self.orientation = None
        self.altitude = None
        self.connections = None

    def __init_map__(self):
        self.mask = None

        # Calc size of the cells
        self.cell_size = 2 * self.cam_altitude * tan(radians(self.underwater_fov_h / 2)) / self.n_cells_h
        rospy.set_param('/{:s}/posidonia/map_posidonia/cell_size'.format(self.robot), self.cell_size)
        print("Cell size: ", self.cell_size)
        # self.v_cell_size = 2 * self.cam_altitude * tan(radians(self.underwater_fov_v / 2)) / self.n_cells_v

        h_map_cells = int(self.h_map_size / self.cell_size)
        v_map_cells = int(self.v_map_size / self.cell_size)

        # Create empty map with size specified
        self.map = np.zeros((v_map_cells, h_map_cells), dtype=[('value', float), ('obs', int)])

        # Create full unexplored map message to send
        self.grid_msg = OccupancyGrid()
        self.grid_msg.header.frame_id = 'posidonia_occupancy_grid'

        # Metadata of the msg
        self.grid_msg.info.origin.position.x = 0
        self.grid_msg.info.origin.position.y = 0
        self.grid_msg.info.resolution = self.cell_size
        self.grid_msg.info.width = h_map_cells
        self.grid_msg.info.height = v_map_cells

        # Create unexplored occupancy grid that will be sent in a msg
        self.grid = np.full((v_map_cells, h_map_cells), fill_value=-1, dtype=np.int8)
        self.grid_msg.data = self.grid.flatten()

        # Send unexplored map. Twice so rviz can get it (once is not enough for some reason).
        for i in range(0, 2):
            self.pub_grid.publish(self.grid_msg)
            self.map_tf_br.sendTransform(
                (-self.h_map_size / 2.0, -self.v_map_size / 2.0, -5),
                tf.transformations.quaternion_from_euler(0, 0, 0), rospy.Time.now(),
                "posidonia_occupancy_grid", "world")

            rospy.Rate(1).sleep()

        self.initial_publish = True

    def read_navsts(self, navsts):
        """ Reads navigation stats from uav """
        if not self.initial_publish:
            return
        self.position = navsts.position
        self.orientation = navsts.orientation
        self.altitude = navsts.altitude

    def read_mask(self, multiarray):
        """ Reads mask message of posidonia and updates the map """
        if self.position is None or self.orientation is None or not self.initial_publish:
            return

        # Convert from UInt8MultiArray msg to np array where data is encoded as binary (0,1 only in our case)
        dims = map(lambda x: x.size, multiarray.layout.dim)  # Get dimensions from the layout group of the msg
        data = [struct.unpack('?', d) for d in multiarray.data]  # Unpack bytes from data as booleans ('?')
        mask = np.array(data, dtype=np.uint8).reshape(dims)  # Transform booleans to uint8 and reshape matrix

        # squish data mask into number of cells required.
        # If the division is not whole, remainder columns and rows will be discarded (right and bottom ones)
        v_radius = mask.shape[0] / self.n_cells_v
        h_radius = mask.shape[1] / self.n_cells_h

        # vertically
        aux1 = np.zeros((self.n_cells_v, mask.shape[1]), float)
        for i in range(0, self.n_cells_v):
            row_idx = i * v_radius
            for j in range(0, v_radius):
                aux1[i] += mask[row_idx + j]

        # Horizontally
        aux2 = np.zeros((self.n_cells_v, self.n_cells_h), float)
        for i in range(0, self.n_cells_v):
            for j in range(0, self.n_cells_h):
                value = 0
                col_idx = j * h_radius
                for k in range(0, h_radius):
                    value += aux1[i][col_idx + k]
                aux2[i][j] = value

        p_matrix = aux2 / (h_radius * v_radius)

        # Calc positional coordinates of the cells of the matrix where the geometrical center is the robot position.
        # Then add to the corresponding values on the map
        x_ini = self.cell_size * self.n_cells_v/2.0
        y_ini = -self.cell_size * self.n_cells_h/2.0
        x_pos = self.position.north
        y_pos = self.position.east
        angle = self.orientation.yaw

        for i in range(0, self.n_cells_v):
            for j in range(0, self.n_cells_h):
                # Get x and y positions relative to the center of the matrix (uav pose)
                x = x_pos + x_ini - i * self.cell_size
                y = y_pos + y_ini + j * self.cell_size

                # Rotate coordinates by yaw to get the real x and y
                x, y = self.rotate(x_pos, y_pos, x, y, angle)

                # Find the closest cell to this coordinates
                map_i, map_j = self.find_map_closest_cell(x, y)

                # Update map with data
                self.map[map_i][map_j]['value'] += p_matrix[i][j]
                self.map[map_i][map_j]['obs'] += 1

    def rotate(self, ori_x, ori_y, px, py, angle):
        """ Rotates counterclockwise point (px, py) around origin(ori_x, ori_y) """
        x = ori_x + cos(angle) * (px - ori_x) - sin(angle) * (py - ori_y)
        y = ori_y + sin(angle) * (px - ori_x) + cos(angle) * (py - ori_y)

        return x, y

    def find_map_closest_cell(self, x, y):
        """ Finds the closest cell on the map to the coordinates provided """

        # Flip coords in origin from standard x,y to NED x,y
        ned_x = -y
        ned_y = x

        # Transform coords from origin in center to origin in top-left
        o_x = ned_x + self.v_map_size / 2
        o_y = ned_y + self.h_map_size / 2

        # Transform from map coords to array index
        i = int(round(o_x / self.cell_size))
        j = int(round(o_y / self.cell_size))

        return i, j

    def update_grid(self):
        """ Updates grid data """
        for row in range(0, self.map.shape[0]):
            for col in range(0, self.map.shape[1]):
                if self.map[row][col]['obs'] > 0:
                    self.grid[row][col] = np.int8(round(100*self.map[row][col]['value']/self.map[row][col]['obs']))

        self.grid_msg.data = self.grid.flatten()

    def send_update_msg(self):
        """ Sends an update message with the local information around the vehicle within a certain radius """
        msg = OccupancyGridUpdate()

        # Find robot position on the map
        map_i = int(round((self.position.north + self.v_map_size / 2) / self.cell_size))
        map_j = int(round((-self.position.east + self.h_map_size / 2) / self.cell_size))

        msg.x = map_i - self.update_radius
        msg.y = map_j - self.update_radius
        msg.width = self.update_radius * 2
        msg.height = msg.width

        data = np.full((msg.width, msg.height), fill_value=-1, dtype=np.int8)
        for row in range(0, msg.height):
            for col in range(0, msg.width):
                cell = self.map[msg.y + row][msg.x + col]
                if cell['obs'] > 0:
                    data[row][col] = np.int8(round(100*cell['value']/cell['obs']))

        msg.data = data.flatten()
        self.pub_upd_grid.publish(msg)


    def iterate(self):
        """ Sends map msg """
        if self.position is not None:
            self.send_update_msg()

            # Broadcast map transform
            self.map_tf_br.sendTransform((-self.h_map_size/2.0, -self.v_map_size/2.0, -(self.position.depth + self.altitude)),
                                         tf.transformations.quaternion_from_euler(0, 0, 0), rospy.Time.now(),
                                         "posidonia_occupancy_grid", "world")

        # Detect new connections and publish the whole map if a new connection is detected
        n_connections = self.pub_grid.get_num_connections()
        if self.connections is None or self.connections < n_connections:
            print("publishing map...")
            self.update_grid()
            self.grid_msg.data = self.grid.flatten()
            self.pub_grid.publish(self.grid_msg)
            print("done!")

        self.connections = n_connections



def print(*args):
    """ Custom print to show name of the node """
    if len(args) == 1:
        __builtin__.print(bold_color + rospy.names.get_name() + ": " + end_color + str(args[0]))
    else:
        __builtin__.print(bold_color + rospy.names.get_name() + ": " + end_color + str(args))


if __name__ == '__main__':
    # default robot
    robot = "girona500"

    # input
    if len(sys.argv) >= 2:
        robot = sys.argv[1]


    # init node
    rospy.init_node("map_posidonia")
    node = MapPosidonia(robot)
    r = float(rospy.get_param("/{:s}/posidonia/map_posidonia/rate".format(robot)))
    rate = rospy.Rate(r)


    while not rospy.is_shutdown():
        node.iterate()
        rate.sleep()
