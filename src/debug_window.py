#!/usr/bin/env python
"""
This node serves as a debug tool to view what the vehicle is seeing and mapping. Also shows the selected candidates
of the exploration
"""

from __future__ import print_function
import sys
import rospy
import numpy as np
import __builtin__

from map_msgs.msg import OccupancyGridUpdate

from math import degrees, atan2, sqrt, pi, sin, cos
from cola2_msgs.msg import NavSts, WorldSectionActionGoal, WorldSectionGoal, WorldSectionActionResult, BodyVelocityReq

debug_mode = bool(rospy.get_param("/posidonia_nav_supervisor_debug_mode"))
if debug_mode:
    import pyglet
    cell_width = 20  # in pixels
    cell_height = 20  # in pixels
    pyglet.resource.path = ["../resources"]
    pyglet.resource.reindex()

bold_color = '\033[1m'
end_color = '\033[0m'

# weighting factor, dist vs angle
weighting_factor = 10

class DebugWindow(pyglet.window.Window):
    def __init__(self, robot):
        super(DebugWindow, self).__init__(600, 600, resizable=True)
        self.init_done = False
        self.robot = robot
        self.__init_ros__()
        self.__init_pyglet__()

        self.init_done = True
        self.candidate = None

    def __init_ros__(self):
        self.update_radius = int(rospy.get_param("/{:s}/posidonia/map_posidonia/update_radius"
                                                 .format(self.robot)))
        self.cell_size_meters = float(rospy.get_param("/{:s}/posidonia/map_posidonia/cell_size".format(robot)))

        self.uav_orientation = None
        self.updated = False

        # Subscriptions
        self.sub_update = rospy.Subscriber("/{:s}/posidonia/occupancy_grid_updates".format(robot),
                                           OccupancyGridUpdate, self.update_map)
        self.sub_navsts = rospy.Subscriber("/{:s}/navigator/navigation".format(robot), NavSts, self.read_navsts)

    def __init_pyglet__(self):
        self.map = None

        self.update_radius = int(rospy.get_param("/{:s}/posidonia/map_posidonia/update_radius"
                                                 .format(self.robot)))

        self.window_shape = (self.update_radius * 2, self.update_radius * 2)

        # load sprites used
        self.floor_img = pyglet.resource.image("floor.png")
        self.posi_90_img = pyglet.resource.image("posidonia_90.png")
        self.posi_75_img = pyglet.resource.image("posidonia_75.png")
        self.posi_50_img = pyglet.resource.image("posidonia_50.png")
        self.posi_25_img = pyglet.resource.image("posidonia_25.png")
        self.posi_10_img = pyglet.resource.image("posidonia_10.png")
        self.unexplored_img = pyglet.resource.image("unexplored.png")
        self.robot_img = pyglet.resource.image("girona500.svg")
        self.candidate_img = pyglet.resource.image("candidate.png")
        self.candidate_ok_img = pyglet.resource.image("candidate_OK.png")
        self.no_candidate_img = pyglet.resource.image("no_candidate.png")

        # Girona is 1.5 x 1.0 meters
        self.robot_img.height = int(round(1.5 * cell_width / self.cell_size_meters))
        self.robot_img.width = int(round(1.0 * cell_width / self.cell_size_meters))

        self.robot_img.anchor_x = self.robot_img.width / 2
        self.robot_img.anchor_y = self.robot_img.height / 2

        # Create sprite batches
        self.ocean_batch = pyglet.graphics.Batch()
        self.uav_batch = pyglet.graphics.Batch()
        self.candidate_batch = pyglet.graphics.Batch()

        # List of sprites associated with batches
        self.sprites = np.zeros(self.window_shape, dtype=pyglet.sprite.Sprite)
        self.candidate_sprites = np.zeros(self.window_shape, dtype=pyglet.sprite.Sprite)
        # fill all sprite cells as unexplored
        x = 0
        y = self.get_size()[1] - cell_height  # Top left corner
        for row in range(0, self.window_shape[0]):
            x = 0
            for col in range(0, self.window_shape[1]):
                self.sprites[row][col] = pyglet.sprite.Sprite(self.unexplored_img, x=x, y=y, batch=self.ocean_batch)
                self.candidate_sprites[row][col] = pyglet.sprite.Sprite(self.no_candidate_img, x=x, y=y, batch=self.candidate_batch)
                x += cell_width
            y -= cell_height

        self.uav_sprite = pyglet.sprite.Sprite(self.robot_img, x=self.update_radius * cell_width,
                                               y=self.update_radius * cell_height, batch=self.uav_batch)

    def update_map(self, update_msg):
        """ Updates the local copy of the map """
        if not self.init_done:
            return
        width = update_msg.width
        height = update_msg.height
        self.map = np.reshape(update_msg.data, (height, width))
        self.map = self.map[::-1, ::-1].T  # Anti transpose matrix
        # map area comes mirrored, mirror again
        # self.map = np.fliplr(self.map)

        if self.map is not None:
            for row in range(0, self.map.shape[0]):
                for col in range(0, self.map.shape[1]):
                    value = self.map[row][col]
                    if value == -1:
                        self.sprites[row][col].image = self.unexplored_img
                    elif value == 0:
                        self.sprites[row][col].image = self.floor_img
                    else:
                        self.sprites[row][col].image = self.posi_90_img

            self.uav_sprite.rotation = degrees(self.uav_orientation.yaw)

        # Find candidates and create sprites for them
        candidates = self.find_candidates()
        center = (height/2.0 + 0.5, width/2.0 + 0.5)
        candidate = self.find_best_candidate(candidates, center)

        # Clean from previous frames
        for row in range(0, self.map.shape[0]):
            for col in range(0, self.map.shape[1]):
                self.candidate_sprites[row][col].image = self.no_candidate_img
        # Paint candidate cells
        for c in candidates:
            self.candidate_sprites[c[0]][c[1]].image = self.candidate_img

        # Paint candidate cell
        if candidate is not None:
            self.candidate_sprites[candidate[0]][candidate[1]].image = self.candidate_ok_img
        self.candidate = candidate

    def find_candidates(self):
        """
        Finds candidates in 8-neighbours surrounding the candidate pixel
        Candidates can be posidonia cells that have as neighbours at least one empty
        cell and one unexplored cell
        """
        candidates = []
        for row in range(1, self.map.shape[0] - 1):
            for col in range(1, self.map.shape[1] - 1):
                # If a posidonia cell
                if self.map[row][col] > 0:
                    # Has as neighbours an empty and an unexplored cell:
                    neighbourhood = self.map[row - 1:row + 2, col - 1:col + 2]
                    if -1 in neighbourhood and 0 in neighbourhood:
                        # It is a candidate

                        # Find candidate angle towards empty cells from center of detection cells and
                        # center of empty cells within the neighbourhood
                        empty_c_x = 0.0
                        empty_c_y = 0.0
                        empty_cont = 0
                        det_c_x = 0.0
                        det_c_y = 0.0
                        det_cont = 0
                        for i, fila in enumerate(neighbourhood):
                            for j, valor in enumerate(fila):
                                if valor == 0:  # If cell is empty
                                    empty_cont += 1
                                    empty_c_x += i
                                    empty_c_y += j
                                if valor > 0:  # If cell has detection
                                    det_cont += 1
                                    det_c_x += i
                                    det_c_y += j

                        # Get center of empty cells
                        empty_c_x = empty_c_x / empty_cont
                        empty_c_y = empty_c_y / empty_cont

                        # Get center of detection cells
                        det_c_x = det_c_x / det_cont
                        det_c_y = det_c_y / det_cont

                        # Angle between the center detection point and surrounding empty space
                        angle = -1*atan2(det_c_y - empty_c_y, det_c_x - empty_c_x)

                        # Filter isolated candidates, probably noise
                        if det_cont > 1:
                            candidates.append((row, col, angle))

        return candidates

    def find_best_candidate(self, candidates, robot_cell):
        """ Following Dr Vidal's paper, apply a weight function to each candidate to find the best one """
        cost = None
        candidate = None
        for can in candidates:
            b = self.wrap_pi(-atan2(robot_cell[1] - can[1], robot_cell[0] - can[0]) - self.uav_orientation.yaw)
            d = sqrt((can[1] - robot_cell[1])**2 + (can[0] - robot_cell[0])**2)
            c = d + weighting_factor * abs(pi/2 - b)/(pi/2)

            if cost is None or c < cost:
                cost = c
                candidate = can

        return candidate

    def read_navsts(self, navsts):
        """ Reads navigation stats from uav """
        # self.position = navsts.position
        self.uav_orientation = navsts.orientation
        # self.altitude = navsts.altitude

    def draw_candidate_viewport(self):
        """ Draws an arrow over the candidate """
        if self.candidate is None:
            return
        s = self.candidate_sprites[self.candidate[0]][self.candidate[1]]
        angle = self.candidate[2]
        point = [s.x + cell_width/2, s.y + cell_height/2]
        self.draw_viewport(point, angle, 60)

    def draw_viewport(self, point, angle, dist):
        """ Draws an arrow, symbolizing the viewpoint """
        p2 = [point[0] + sin(angle)*dist, point[1] + cos(angle)*dist]

        pyglet.gl.glBegin(pyglet.gl.GL_LINES)
        pyglet.gl.glVertex2f(point[0], point[1])
        pyglet.gl.glVertex2f(p2[0], p2[1])

        beta = angle - pi/2
        # Draw an X over the second point
        x1 = [p2[0] + sin(beta - pi/4)*10, p2[1] + cos(beta - pi/4)*10]
        x2 = [p2[0] + sin(beta + 3*pi/4)*10, p2[1] + cos(beta + 3*pi/4)*10]
        x3 = [p2[0] + sin(beta - 3*pi/4)*10, p2[1] + cos(beta - 3*pi/4)*10]
        x4 = [p2[0] + sin(beta + pi/4)*10, p2[1] + cos(beta + pi/4)*10]

        pyglet.gl.glVertex2f(x1[0],x1[1])
        pyglet.gl.glVertex2f(x2[0],x2[1])
        pyglet.gl.glVertex2f(x3[0],x3[1])
        pyglet.gl.glVertex2f(x4[0],x4[1])

        # Draw arrow pointing beta
        p3 = [p2[0] + sin(beta)*dist/2, p2[1] + cos(beta)*dist/2]
        a1 = [p3[0] + sin(beta - 3*pi/4)*10, p3[1] + cos(beta - 3*pi/4)*10]
        a2 = [p3[0] + sin(beta + 3*pi/4)*10, p3[1] + cos(beta + 3*pi/4)*10]
        pyglet.gl.glVertex2f(p2[0],p2[1])
        pyglet.gl.glVertex2f(p3[0],p3[1])

        pyglet.gl.glVertex2f(p3[0],p3[1])
        pyglet.gl.glVertex2f(a1[0],a1[1])

        pyglet.gl.glVertex2f(a1[0],a1[1])
        pyglet.gl.glVertex2f(a2[0],a2[1])

        pyglet.gl.glVertex2f(a2[0],a2[1])
        pyglet.gl.glVertex2f(p3[0],p3[1])

        pyglet.gl.glEnd()

    def on_draw(self):
        """ Called every frame to draw the scene """
        self.clear()
        self.ocean_batch.draw()
        self.uav_batch.draw()
        self.candidate_batch.draw()
        self.draw_candidate_viewport()

    def wrap_pi(self, angle):
        """ Converts an angle to a value contained within the range (-pi, pi] """
        angle = angle % (2*pi)
        angle = (angle + 2*pi) % (2*pi)
        if angle > pi:
            angle -= 2*pi

        return angle


def print(*args):
    """ Custom print to show name of the node """
    if len(args) == 1:
        __builtin__.print(bold_color + rospy.names.get_name() + ": " + end_color + str(args[0]))
    else:
        __builtin__.print(bold_color + rospy.names.get_name() + ": " + end_color + str(args))


def update(dt):
    pass


if __name__ == '__main__':
    # default robot
    robot = "girona500"

    # input
    if len(sys.argv) <= 2:
        robot = sys.argv[1]

    if debug_mode:
        rospy.init_node("debug_window")
        rate = rospy.Rate(10.0)
        # Wait for the map to initialise
        while not bool(rospy.get_param("/{:s}/posidonia/map_posidonia/init_done".format(robot))):
            rate.sleep()
        print("Creating debug window")
        node = DebugWindow(robot)
        pyglet.clock.schedule_interval(update, .2)
        pyglet.app.run()
