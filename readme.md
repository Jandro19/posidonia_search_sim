## SEARCH POSIDONIA MODULE
This package is designed to follow the edge of posidonia meadow using the Girona500 autonomous underwater vehicle inside a realistic simulation.
It is the result of my final degree thesis.

This package utilises the following open-source libraries:
* [COLA2](https://bitbucket.org/iquarobotics/) from IQUA Robotics
* [Stonefish](https://github.com/patrykcieslak/stonefish) and [Stonefish_ros](https://github.com/patrykcieslak/stonefish_ros) from Patryk Cieslak

This package uses assets from the open-source [cola2_stonefish](https://bitbucket.org/udg_cirs/cola2_stonefish/src/master/) package from cirs_udg.
